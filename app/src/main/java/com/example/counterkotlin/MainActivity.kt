package com.example.counterkotlin

import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var mThread: Thread? = null
    @Volatile
    private var number = 0
    @Volatile
    private var isRunning = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_inc.setOnTouchListener(object : OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_DOWN) {
                    number++
                    txt_number!!.text = "" + number
                    stopThread()
                    return false
                }
                if (event.action == MotionEvent.ACTION_UP) {
                    startNewThread()
                    return false
                }
                return false
            }
        })
        btn_dec.setOnTouchListener(object : OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                if (event.action == MotionEvent.ACTION_DOWN) {
                    number--
                    txt_number!!.text = "" + number
                    stopThread()
                    return false
                }
                if (event.action == MotionEvent.ACTION_UP) {
                    startNewThread()
                    return false
                }
                return false
            }
        })
    }
    private var y1 = 0f
    private var y2 = 0f
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_UP -> {
                startNewThread()
                return false
            }
            MotionEvent.ACTION_DOWN -> {
                y1 = event.y
                stopThread()
                return false
            }
            MotionEvent.ACTION_MOVE -> {
                y2 = event.y
                if ((y1 - y2) / 5 >= 1) {
                    y1 = y2
                    txt_number?.text =  (number++).toString()
                }
                if ((y2 - y1) / 5 >= 1) {
                    y1 = y2
                    txt_number?.text =  (number--).toString()
                }
            }
        }
        return super.onTouchEvent(event)
    }

    fun startNewThread(){
        synchronized(this) { isRunning = true }
        if (mThread == null) {
            mThread = Thread(runnable, "ThanhLV")
            mThread?.start()
        }
    }
    fun stopThread(){
        synchronized(this) { isRunning = false }
        if (mThread != null) {
            mThread?.interrupt()
            mThread = null
        }
    }

    var runnable = Runnable {
        while (isRunning) {
            if (number > 0) {
                if (!isRunning) break
                try {
                    Thread.sleep(1500)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                while (number > 0) {
                    number--
                    runOnUiThread { txt_number?.text = number.toString() }
                    if (!isRunning) break
                    try {
                        Thread.sleep(50)
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                }
            }
            if (number < 0) {
                if (!isRunning) break
                try {
                    Thread.sleep(1500)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
                while (number < 0) {
                    number++
                    runOnUiThread { txt_number?.text = number.toString() }
                    if (!isRunning) break
                    try {
                        Thread.sleep(50)
                    } catch (e: InterruptedException) {
                        e.printStackTrace()
                    }
                }
            }
            if (number == 0) {
                runOnUiThread { txt_number?.text = "0" }
            }
        }
    }
}